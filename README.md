Yext JSON API
=====

Install this module to ensure your drupal site is ready to use the Yext data connectors.

About Answers
-----

Yext Answers is a revolutionary site search product that understands natural 
language and puts dynamic answers on your website to help consumers convert — 
right in the search results.

[Build On Yext - Your Way](https://www.youtube.com/watch?v=sXdb4Qmc9Z0)

